$(document).ready(function(){    
    $('#menu-toggle').on('click',function(){
        $('.main-menu').removeClass('hide');
    });
    $('#menu-close').on('click',function(){
        $('.main-menu').addClass('hide');
    });

    $('.tab-enjoy').click(function(){
        var tab_selection = $(this).attr('data');
        var tab_selector = $(this).attr('selector');
        var tab_name = $(this).attr('tab-name');
        // console.log(tab_selection);

        $('.'+tab_selector+' .tab-enjoy').removeClass(tab_name);
        $(this).addClass(tab_name);

        $('.'+tab_selector+' .tab-desc').addClass('hide');
        $('.'+tab_selector+' .'+tab_selection).removeClass('hide');
    });

    $('#more-desc').hide();

    $('.toggle-readmore').on('click', function(){       
        var more_div_selection = $(this).attr('selector');
        
        $('.'+more_div_selection+' #more-desc').slideToggle();
        $('.'+more_div_selection+' #arrow-toggle').toggleClass('arrow-reverse');
    });

    $("#player").hide();
    $("#play").on("click", function(e) {
        e.preventDefault();
        $("#player")[0].src += "?autoplay=1";
        $("#player").show();
        $("#video-cover").hide();
        $("#play").hide();
    });

    $(document).delegate('.play_video_intern', 'click', function() {
        if (video.paused == true) {
            video.play();
            $(this).css('opacity',0);
        } else {
            video.pause();
            $(this).css('opacity',9);
            // $(this).html("<i class='fa fa-play'></i>");
        }
    }); 
});